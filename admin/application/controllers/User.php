<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		else {
			$profile = $this->router->fetch_method();
			if($profile != 'profile') {
			$menu = $this->session->userdata('admin');
			 if( $menu!=1  ) {
				 $this->session->set_flashdata('message', array('message' => "You don't have permission to access user page.",'class' => 'danger'));
				 redirect(base_url().'dashboard');
			 }
			}
		}
 	}
	
	public function profile() {
		
		$template['page'] = 'User/profile';
		$template['page_title'] = "Member Profile";
		$id = $this->session->userdata('logged_in')['id'];
		$template['data'] = $this->user_model->get_single_profile($id);
		if($_POST) {
			$data = $_POST;
			unset($data['submit']);
			
			if(isset($_FILES['profile_pic'])) {
				$config = $this->set_upload_options();
			
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				if ( ! $this->upload->do_upload('profile_pic')) {
					unset($data['profile_pic']);
				}
				else {
					$data['profile_pic'] = $config['upload_path']."/".$_FILES['profile_pic']['name'];
				}
			}
			
			$result = $this->user_model->update_user($data, $id);
			if($result == "Exist") {
			$this->session->set_flashdata('message', array('message' => 'Owner already exist','class' => 'danger'));
			   }
				
			else {	
			array_walk($data, "remove_html");
			$this->session->set_flashdata('message', array('message' => 'Profile Updated Successfully','class' => 'success'));
			   }
			
			
     		redirect(base_url().'user/profile');
		}
		else {
   			$this->load->view('template', $template);
		}
		
	}
	
	public function add_user() {
	
		$template['page'] = 'User/add-user';
		$template['page_title'] = "Add Member";
	      
	    if($_POST) {
			$data = $_POST;
			unset($data['submit']);
			
			$config = $this->set_upload_options();
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('profile_pic')) {
				//$result = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('message', array('message' => 'Error Occured While Uploading Files','class' => 'danger'));
				//echo $this->upload->display_errors();
			}
			else {
				//$data['profile_pic'] = base_url().$config['upload_path']."/".$_FILES['profile_pic']['name'];
				$upload_data = $this->upload->data();
				$data['profile_pic'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
				$data['password'] = md5($data['password']);
				$result = $this->user_model->save_member($data);
				
				if($result == "Exist") {
					$this->session->set_flashdata('message', array('message' => 'Member already exist','class' => 'danger'));
				}
				
				else {
					/*if($notify != '') {
						// Send Mail
						$this->load->library('email');
						// prepare email
						$this->email
							->from("no-reply@bookmysalons.com", "BookMySalons")
							->to($data['email_id'])
							->subject('Welcome to BookMySalons')
							->message($this->load->view('customer-email-template', $data, true))
							->set_mailtype('html');
						
						// send email
						$result = $this->email->send();
					}
					array_walk($data, "remove_html");*/
					$this->session->set_flashdata('message', array('message' => 'Member Saved successfully','class' => 'success'));
				}
				
	     		redirect(base_url().'user/view_user');	
			}
		}
		else {
   			$this->load->view('template', $template);
		}
	}

  	public function view_user() {
		$template['page'] = 'User/view-user';
		$template['page_title'] = "View Member";
		$template['data'] = $this->user_model->get_user();
		$this->load->view('template',$template);
	}
	
	public function edit_user() {
		
		$template['page'] = 'User/edit-user';
		$template['page_title'] = "Edit Member";
		
		$id = $this->uri->segment(3);
		$template['data'] = $this->user_model->get_single_user($id);
		if($_POST) {
			$data = $_POST;
			
			unset($data['submit']);
			
			$result = $this->user_model->update_limit($data, $id);
			array_walk($data, "remove_html");
			$this->session->set_flashdata('message', array('message' => 'Member Updated Successfully','class' => 'success'));
     		redirect(base_url().'user/view_user');
		}
		else {
   			$this->load->view('template', $template);
		}
	}
	
	public function view_single_user() {
		$id = $_POST['id'];
		$template['data'] = $this->user_model->get_single_user($id);
		$this->load->view('User/view-user-popup',$template);
	}
	
	private function set_upload_options() {   
		//upload an image options
		$config = array();
		$config['upload_path'] = 'assets/uploads/profile_pic';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = '5000';
		$config['overwrite']     = FALSE;
	
		return $config;
	}

}	

