<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technician extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('technician_model');
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		else {
			$menu = $this->session->userdata('admin');
			 if( $menu!=1  ) {
				 $this->session->set_flashdata('message', array('message' => "You don't have permission to access technicians page.",'class' => 'danger'));
				 redirect(base_url().'dashboard');
			 }
		}
 	}
	
	public function add_technician() {
	
	$template['page'] = 'Technician/add-technician';
	$template['page_title'] = "Add Technician";
	      
	      if($_POST) {
			$data = $_POST;
			unset($data['submit']);
			
			$notify = '';
			if(isset($data['nofity'])) {
				$notify = $data['nofity'];
				unset($data['notify']);
			}
			
			$config = $this->set_upload_options();
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('profile_pic')) {
				//$result = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('message', array('message' => 'Error Occured While Uploading Files','class' => 'danger'));
				//echo $this->upload->display_errors();
			}
			else {
			
			//$data['profile_pic'] = base_url().$config['upload_path']."/".$_FILES['profile_pic']['name'];
			$upload_data = $this->upload->data();
			$data['profile_pic'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
			$data['type'] = 0;
			$result = $this->technician_model->save_technician($data);
			
			if($result == "Exist") {
				$this->session->set_flashdata('message', array('message' => 'Technician already exist','class' => 'danger'));
			}
			
			else {
				if($notify != '') {
					// Send Mail
					$this->load->library('email');
					// prepare email
					$this->email
						->from("no-reply@bookmysalons.com", "BookMySalons")
						->to($data['email_id'])
						->subject('Welcome to BookMySalons')
						->message($this->load->view('customer-email-template', $data, true))
						->set_mailtype('html');
					
					// send email
					$result = $this->email->send();
				}
				array_walk($data, "remove_html");
				$this->session->set_flashdata('message', array('message' => 'Technician Saved successfully','class' => 'success'));
			}
			
     		redirect(base_url().'technician/view_technician');
		}
		  }
		else {
   			$this->load->view('template', $template);
		}
	}

  public function view_technician() {
		$template['page'] = 'Technician/view-technician';
		$template['page_title'] = "View Technician";
		$template['data'] = $this->technician_model->get_technician();
		$this->load->view('template',$template);
	}
public function view_single_technician() {
		$id = $_POST['id'];
		$template['data'] = $this->technician_model->get_single_technician($id);
		$this->load->view('Technician/view-technician-popup',$template);
	}
 
 public function delete_technician() {
		$id = $this->uri->segment(3);
		$result = $this->technician_model->delete_technician($id);
		$this->session->set_flashdata('message', array('message' => 'Technician Deleted Successfully','class' => 'success'));
     	redirect(base_url().'technician/view_technician');
	}
 
 public function edit_technician() {
		
		$template['page'] = 'Technician/edit-technician';
		$template['page_title'] = "Edit Technician";
		
		$id = $this->uri->segment(3);
		$template['data'] = $this->technician_model->get_single_technician($id);
		if($_POST) {
			$data = $_POST;
			
			unset($data['submit']);
			$notify = '';
			if(isset($data['nofity'])) {
			$notify = $data['nofity'];
			unset($data['notify']);
			}
			if(isset($_FILES['profile_pic'])) {
				$config = $this->set_upload_options();
			
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				if ( ! $this->upload->do_upload('profile_pic')) {
					unset($data['profile_pic']);
				}
				else {
					$upload_data = $this->upload->data();
					$data['profile_pic'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
					//$data['profile_pic'] = base_url().$config['upload_path']."/".$_FILES['profile_pic']['name'];
				}
			}
			$result = $this->technician_model->update_technician($data, $id);
			
			if($result == "Exist") {
				$this->session->set_flashdata('message', array('message' => 'Technician already exist','class' => 'danger'));
			   }
			else if($result == "Already Exist") {
				$this->session->set_flashdata('message', array('message' => 'Email id already exist','class' => 'danger'));
			   }
				
			else {
				if($notify != '') {
					// Send Mail
					$this->load->library('email');
					// prepare email
					$this->email
						->from("no-reply@bookmysalons.com", "BookMySalons")
						->to($data['email_id'])
						->subject('Welcome to BookMySalons')
						->message($this->load->view('customer-email-template', $data, true))
						->set_mailtype('html');
					
					// send email
					$result = $this->email->send();
				}
				array_walk($data, "remove_html");
				$this->session->set_flashdata('message', array('message' => 'Technician Updated Successfully','class' => 'success'));
			   }
			
		
     		redirect(base_url().'technician/view_technician');
		}
		else {
   			$this->load->view('template', $template);
		}
	}

 private function set_upload_options() {   
		//upload an image options
		$config = array();
		$config['upload_path'] = 'assets/uploads/profile_pic';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '5000';
		$config['overwrite']     = FALSE;
	
		return $config;
	}
 
}

