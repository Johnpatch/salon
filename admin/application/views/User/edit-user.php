
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> <?php echo $page_title; ?></h2>

            </div>
            <div class="box-content">
                <form role="form" method="post" class="validate" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label" for="business_name">Booking Limit</label>
                        <input type="number" name="limit" value="<?php echo $data->limit; ?>" class="form-control required" placeholder="Enter Booking Limit">
                    </div>
                    
                    
                    <button type="submit" class="btn btn-custom"><i class="glyphicon glyphicon-plus"></i> Update Member</button>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->


